package com.prosa.ws.mock.repository;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;

import mx.com.prosa.ws.mock.swa.FindAllResponse;
import mx.com.prosa.ws.mock.swa.MapEntry;

@Component
public class OpenSwitchRepository {

	private static final Map<String, String> swaMap = new HashMap<>();

	@PostConstruct
	public void init() {
		swaMap.put("uno", "Prueba de datos");
	}

	public String getResponse(String key) {
		return swaMap.getOrDefault(key, "Data not found '>_<'");
	}

	public void addData(String key, String value) {
		swaMap.put(key, value);
	}

	public FindAllResponse findAll() {
		FindAllResponse response = new FindAllResponse();
		swaMap.forEach((k, v) -> {
			MapEntry mapEntry = new MapEntry();
			mapEntry.setKey(k);
			mapEntry.setValue(v);
			response.getMapEntry().add(mapEntry);
		});
		return response;
	}

}
