package com.prosa.ws.mock.utilities;

import java.util.Optional;
import java.util.regex.Pattern;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class Util {

	private static final Logger LOGGER = LoggerFactory.getLogger(Util.class);

	public String convertObjectToJson(Object object) {
		return ToStringBuilder.reflectionToString(object, ToStringStyle.JSON_STYLE);
	}

	public void validateData(String... fields) {
		for (String field : fields) {
			if (!Optional.ofNullable(field).isPresent()) {
				LOGGER.error("{}", "Some field is null or empty");
				throw new NullPointerException("Some field is null or empty");
			}
		}
	}

	public void validateInfoKey(String key) {
		if (!Pattern.matches(Constants.REGEX_KEY, key)) {
			LOGGER.error("The key field with value {} does not meet the expected format (.*-[1-3])", key);
			throw new IllegalArgumentException("The key field does not meet the expected format (.*-[1-3])");
		}
	}

}
