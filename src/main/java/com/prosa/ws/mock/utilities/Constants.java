package com.prosa.ws.mock.utilities;

public class Constants {

	public static final String NAMESPACE_URI = "http://prosa.com.mx/ws/mock/swa";
	public static final String FILE_NAME = "swa.xsd";
	public static final String LOCATION_URI = "/ws/mock-swa";
	public static final String TARGET_NAME_SPACE_URI = "http://prosa.com.mx/ws/mock/swa";
	public static final String ULR_MAPPING = "/ws/*";

	public static final String REGEX_KEY = ".*-[1-3]";

	private Constants() {
		throw new IllegalStateException("Constants class");
	}

}
