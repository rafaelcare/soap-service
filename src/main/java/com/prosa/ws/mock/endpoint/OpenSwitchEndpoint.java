package com.prosa.ws.mock.endpoint;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.prosa.ws.mock.repository.OpenSwitchRepository;
import com.prosa.ws.mock.utilities.Constants;
import com.prosa.ws.mock.utilities.Util;

import mx.com.prosa.ws.mock.swa.AddRequest;
import mx.com.prosa.ws.mock.swa.AddResponse;
import mx.com.prosa.ws.mock.swa.FindAllRequest;
import mx.com.prosa.ws.mock.swa.FindAllResponse;
import mx.com.prosa.ws.mock.swa.SwaRequest;
import mx.com.prosa.ws.mock.swa.SwaResponse;

@Endpoint
public class OpenSwitchEndpoint {

	private static final Logger LOGGER = LoggerFactory.getLogger(OpenSwitchEndpoint.class);

	@Autowired
	private OpenSwitchRepository openSwitchRepository;

	@Autowired
	private Util util;

	@PayloadRoot(namespace = Constants.NAMESPACE_URI, localPart = "swaRequest")
	@ResponsePayload
	public SwaResponse getDataByKey(@RequestPayload SwaRequest request) {
		LOGGER.info("SwaRequest getDataByKey {}", util.convertObjectToJson(request));
		SwaResponse response = new SwaResponse();
		util.validateData(request.getKey(), request.getKeyData());
		util.validateInfoKey(request.getKey());
		response.setResponse(openSwitchRepository.getResponse(request.getKey()));
		return response;
	}

	@PayloadRoot(namespace = Constants.NAMESPACE_URI, localPart = "addRequest")
	@ResponsePayload
	public AddResponse addResponseSwa(@RequestPayload AddRequest request) {
		LOGGER.info("addResponseSwa {}", util.convertObjectToJson(request));
		util.validateData(request.getKey(), request.getKeyData());
		util.validateInfoKey(request.getKey());
		openSwitchRepository.addData(request.getKey(), request.getKeyData());

		AddResponse response = new AddResponse();
		response.setSuccess(Boolean.TRUE);

		return response;
	}

	@PayloadRoot(namespace = Constants.NAMESPACE_URI, localPart = "findAllRequest")
	@ResponsePayload
	public FindAllResponse findAll(@RequestPayload FindAllRequest request) {
		LOGGER.info("{}", "findAll");
		return openSwitchRepository.findAll();
	}

}
