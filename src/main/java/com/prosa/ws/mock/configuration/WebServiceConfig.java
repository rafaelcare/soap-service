package com.prosa.ws.mock.configuration;

import com.prosa.ws.mock.utilities.Constants;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.config.annotation.WsConfigurerAdapter;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;

@EnableWs
@Configuration
public class WebServiceConfig extends WsConfigurerAdapter {

    @Bean
    public ServletRegistrationBean<MessageDispatcherServlet> messageDispatcherServlet(
            ApplicationContext applicationContext) {
        MessageDispatcherServlet servlet = new MessageDispatcherServlet();
        servlet.setApplicationContext(applicationContext);
        servlet.setTransformWsdlLocations(true);
        return new ServletRegistrationBean<>(servlet, Constants.ULR_MAPPING);
    }

    @Bean(name = "swa")
    public static DefaultWsdl11Definition wsdlDefinition(XsdSchema swaSchema) {
        DefaultWsdl11Definition wsdl = new DefaultWsdl11Definition();
        wsdl.setPortTypeName("OpenSwitchPort");
        wsdl.setLocationUri(Constants.LOCATION_URI);
        wsdl.setTargetNamespace(Constants.TARGET_NAME_SPACE_URI);
        wsdl.setSchema(swaSchema);
        return wsdl;
    }

    @Bean
    public XsdSchema swaSchema() {
        return new SimpleXsdSchema(new ClassPathResource(Constants.FILE_NAME));
    }

}
