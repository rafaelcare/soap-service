package com.prosa.ws.mock;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebServiceMockApplication {

    public static void main(String[] args) {
        SpringApplication.run(WebServiceMockApplication.class, args);
    }

}
