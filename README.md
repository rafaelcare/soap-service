# Get WSDL Definition

To get WSDL definition is necessary run the applicattion **(soap-service)** and then invoke the following URL from the browser:
	
	http://localhost:8080/ws/swa.wsdl
